#include <stdbool.h>

typedef struct nodo {
  int dato;
  struct nodo *sig;
} nodo;

typedef struct {
  nodo *primerNodo;
} lista;

lista *crearLista();

nodo *agregarDato(lista *lista, int dato);

void imprimirLista(lista *lista);

bool eliminarDato(lista *lista, int dato);
