#include <stdio.h>
#include "lista_enlazada.h"

int main() {
  lista *lista = crearLista();
  agregarDato(lista, 1255);
  agregarDato(lista, 1322);
  agregarDato(lista, 1008);
  agregarDato(lista, 1304);

  imprimirLista(lista);

  printf("----------------------------------------------\n");
  // Elimino un elemento del medio de la lista
  bool resultadoDeEliminacion = eliminarDato(lista, 1008);
  if (resultadoDeEliminacion) {
    printf("Dato 1008 eliminado\n");
  }
  printf("Lista luego de la eliminacion:\n");
  imprimirLista(lista);

  // Elimino el elemento al principio de la lista
  resultadoDeEliminacion = eliminarDato(lista, 1255);
  if (resultadoDeEliminacion) {
    printf("Dato 1255 eliminado\n");
  }
  printf("Lista luego de la eliminacion:\n");
  imprimirLista(lista);

  // Elimino el elemento al final de la lista
  resultadoDeEliminacion = eliminarDato(lista, 1304);
  if (resultadoDeEliminacion) {
    printf("Dato 1304 eliminado\n");
  }
  printf("Lista luego de la eliminacion:\n");
  imprimirLista(lista);

  // Elimino el ultimo que queda en la lista
  resultadoDeEliminacion = eliminarDato(lista, 1322);
  if (resultadoDeEliminacion) {
    printf("Dato 1322 eliminado\n");
  }
  printf("Lista luego de la eliminacion:\n");
  imprimirLista(lista);
}
