#include <stdlib.h>
#include <stdio.h>
#include "lista_enlazada.h"

lista *crearLista() {
  lista *nuevaLista = (lista*)malloc(sizeof(lista));

  return nuevaLista;
}

nodo *agregarDato(lista *lista, int dato) {
  nodo *nuevoNodo = (nodo*)malloc(sizeof(nodo));
  nuevoNodo->dato = dato;

  // Si la lista aun no tiene elementos, agregar el nuevo nodo al principio
  if (lista->primerNodo == NULL) {
    lista->primerNodo = nuevoNodo;
  }
  else {
    // Buscar el ultimo nodo y agregar el nuevo nodo a continuacion
    nodo *ultimoNodo = lista->primerNodo;
    while (ultimoNodo->sig != NULL) {
      ultimoNodo = ultimoNodo->sig;
    }

    ultimoNodo->sig = nuevoNodo;
  }

  return nuevoNodo;
}

void imprimirLista(lista *lista) {
  // Primero chequeamos que la lista no este vacia
  if (lista->primerNodo == NULL) {
    printf("La lista esta vacia\n");
    return;
  }

  // Ahora recorremos la lista nodo a nodo
  nodo *nodoActual = lista->primerNodo;
  while (nodoActual != NULL) {
    printf("Dato: %d\n", nodoActual->dato);
    nodoActual = nodoActual->sig;
  }
}

bool eliminarDato(lista *lista, int dato) {
  // Primero chequeamos que la lista no este vacia
  if (lista->primerNodo == NULL) {
    return false;
  }

  bool datoEliminado = false;

  // Sino recorremos la lista nodo a nodo buscando el elemento a eliminar
  nodo *nodoActual = lista->primerNodo;
  nodo *nodoAnterior = NULL;

  while (nodoActual != NULL) {
    if (nodoActual->dato == dato) {
      // Chequeo que el nodo a eliminar sea el primer nodo
      if (nodoAnterior == NULL) {
        lista->primerNodo = nodoActual->sig;
      }
      else {
        // Es un nodo en el medio o el ultimo de la lista
        nodoAnterior->sig = nodoActual->sig;
      }
      free(nodoActual);
      nodoActual = NULL;
      datoEliminado = true;
    }
    else {
      // Avanzo al siguiente nodo, reapuntando los punteros a el actual y el anterior
      nodoAnterior = nodoActual;
      nodoActual = nodoActual->sig;
    }
  }

  return datoEliminado;
}
